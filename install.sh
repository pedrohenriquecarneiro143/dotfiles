#! /bin/bash

#Setting dnf
echo "fastestmirror=True" >> /etc/dnf/dnf.conf
echo "max_parallel_downloads=10" >> /etc/dnf/dnf.conf

#using the instalation archives
source ./gnome/app.sh

#Install ASDF
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.11.3

#Install OH MY FISH
curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish

#importing the dotfiles
#gitconfig
ln -s ./git/gitconfig $HOME/.gitconfig
#kitty
ln -s ./kitty/kitty.conf $HOME/.config/kitty/kitty.conf
#fish shell
ln -s ./fish/config.fish $HOME/.config/fish/config.fish
